//
//  ACCategoryModel.h
//  ASAclub
//
//  Created by Rodrigo Mazzilli on 8/3/14.
//  Copyright (c) 2014 Moolab. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ACCategoryModel : NSObject

@property (nonatomic, copy, readonly) NSNumber *categoryId;
@property (nonatomic, strong, readonly) NSArray *members;

- (instancetype)initWithCategoryId:(NSNumber *)categoryId;
- (void)loadModelWithSuccess:(void (^)(NSArray *members))success failure:(void (^)(NSError * __autoreleasing))failure;

@end
