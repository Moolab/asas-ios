//
//  ACError.m
//  ASAclub
//
//  Created by Rodrigo Mazzilli on 7/27/14.
//  Copyright (c) 2014 Moolab. All rights reserved.
//

#import "ACError.h"

NSString *const ACErrorDomain = @"br.moolab.asaclub";

@implementation ACError

@end
