//
//  ACCategory.h
//  ASAclub
//
//  Created by Rodrigo Mazzilli on 7/29/14.
//  Copyright (c) 2014 Moolab. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UIImage;

@interface ACCategory : NSObject

@property (nonatomic, assign, readonly) int categoryId;
@property (nonatomic, copy, readonly) NSString *name;
@property (nonatomic, copy, readonly) NSURL *iconURL;

- (instancetype)initWithCategoryId:(int)categoryId name:(NSString * const)name iconURL:(NSURL * const)iconURL;

@end
