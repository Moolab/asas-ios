//
//  ACCategoryTableViewController.m
//  ASAclub
//
//  Created by Rodrigo Mazzilli on 7/26/14.
//  Copyright (c) 2014 Moolab. All rights reserved.
//

#import "ACCategoryTableViewController.h"
#import "ACCategoryTableViewCell.h"
#import "ACCategoryModel.h"
#import "ACPersistenceManager.h"

#import "MRProgress.h"
#import "UIImageView+AFNetworking.h"

@interface ACCategoryTableViewController ()

@property (nonatomic, strong, readwrite) ACCategoryModel *model;

@end

@implementation ACCategoryTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = self.categoryName;
    // Start listening to synchronization notifications.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didSynchronizeModel) name:ACPersistenceManagerDidSynchronizeDataNotification object:nil];
    // Create model.
    self.model = [[ACCategoryModel alloc] initWithCategoryId:self.categoryId];
    [self loadModel];
}

- (void)loadModel {
    [self.model loadModelWithSuccess:^(NSArray *members) {
        [MRProgressOverlayView dismissOverlayForView:self.view animated:YES];
        [self.tableView reloadData];
        
    } failure:^(NSError *error) {
        [MRProgressOverlayView dismissOverlayForView:self.view animated:YES];
        
    }];
    [MRProgressOverlayView showOverlayAddedTo:self.view animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 8;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ACCategoryTableViewCell *cell = (ACCategoryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:ACCategoryTableViewCellIdentifier forIndexPath:indexPath];
    return cell;
}

#pragma mark - Private

- (void)didSynchronizeModel {
    [self loadModel];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
