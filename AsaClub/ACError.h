//
//  ACError.h
//  ASAclub
//
//  Created by Rodrigo Mazzilli on 7/27/14.
//  Copyright (c) 2014 Moolab. All rights reserved.
//

@import Foundation;

FOUNDATION_EXPORT NSString *const ACErrorDomain;

enum {
    ACUserNotLoggedInError = 1000,
    ACUserLogoutFailedError,
    ACBadServerResponseError
};

@interface ACError : NSObject

@end
