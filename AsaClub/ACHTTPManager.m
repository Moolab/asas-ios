//
//  ACHTTPManager.m
//  ASAclub
//
//  Created by Rodrigo Mazzilli on 7/26/14.
//  Copyright (c) 2014 Moolab. All rights reserved.
//

#import "ACHTTPManager.h"
#import "ACError.h"

NSString * const ACMembershipImageAddress                       =   @"http://www.asaclub.org.br/as_data/imagem_convenio";
NSString * const kACHostAddress                                 =   @"https://www.asaclub.org.br/asa/mobile";

/**
 Helper function to check whether an NSString has any value.
 */
BOOL ACIsStringWithAnyText(id object)
{
    return [object isKindOfClass:[NSString class]] && [(NSString*)object length] > 0;
}

static NSString * ACApplicationName(void)
{
    NSBundle *appBundle = [NSBundle mainBundle];
    id appName = [appBundle objectForInfoDictionaryKey:@"CFBundleDisplayName"];
    if ( [appName isKindOfClass:[NSString class]] && ACIsStringWithAnyText(appName) )
    {
        return appName;
    }
    return @"?";
}

static NSString * ACApplicationBundleShortVersionString(void)
{
	NSBundle *appBundle = [NSBundle mainBundle];
	return (NSString *)[appBundle objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
}

static NSString * ACDefaultUserAgent(void)
{
    return [NSString stringWithFormat:@"%@/%@ (Apple; %@; %@; %@)",
            ACApplicationName(),
            ACApplicationBundleShortVersionString(),
            [[UIDevice currentDevice] model],
            [[UIDevice currentDevice] systemName],
            [[UIDevice currentDevice] systemVersion], nil];
}


@interface ACHTTPManager ()

@property (nonatomic, strong, readwrite) AFHTTPSessionManager *sessionManager;
@property (nonatomic, strong, readwrite) AFNetworkReachabilityManager *reachabilityManager;

@end

@implementation ACHTTPManager

+ (instancetype)sharedInstance
{
    static ACHTTPManager *sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[ACHTTPManager alloc] init];
    });
    return sharedInstance;
}

- (id)init
{
    if ( (self = [super init]) )
    {
        // AFNetworking
        NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
        sessionConfiguration.HTTPAdditionalHeaders = @{ @"User-Agent": ACDefaultUserAgent() };
        self.sessionManager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:kACHostAddress] sessionConfiguration:sessionConfiguration];
        self.sessionManager.requestSerializer = [AFJSONRequestSerializer serializerWithWritingOptions:NSJSONWritingPrettyPrinted];
        self.sessionManager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingMutableContainers];
        
        // Reachability
//        NSURL *URL = [NSURL URLWithString:kACHostAddress];
        self.reachabilityManager = [AFNetworkReachabilityManager managerForDomain:@"google.com"];
    }
    return self;
}

- (void)obtainCategoriesWithSuccess:(void (^)(NSArray *categories))success failure:(void (^)(NSError * __autoreleasing))failure {
    NSError *error = nil;
    NSMutableURLRequest *request = [self.sessionManager.requestSerializer requestWithMethod:@"GET" URLString:kACHostAddress parameters:@{ @"apcacat": @(1) } error:&error];
    __block NSError *newError = nil;
    if ( !error ) {
        NSURLSessionDataTask *task = [self.sessionManager dataTaskWithRequest:request completionHandler:^(NSURLResponse * __unused response, id responseObject, NSError *error) {
            NSAssert(error == nil, @"an error occurred while obtaining categories");
            if (error) {
                NSLog(@"an error occurred while obtaining categories; %@", error);
                if (failure) {
                    failure(error);
                }
                
            } else {
                // Let's make sure we received an array.
                if ( [responseObject isKindOfClass:[NSArray class]] ) {
                    if ( success )
                    {
                        success(responseObject);
                    }
                    
                } else {
                    // Server responded with an expected type.
                    NSLog(@"server returned an unexpected type of data in request to obtain categories; must receive an array");
                    if ( failure ) {
                        newError = [[NSError alloc] initWithDomain:ACErrorDomain code:ACBadServerResponseError userInfo:nil];
                        failure(newError);
                    }
                }
            }
        }];
        [task resume];
        
    } else {
        NSLog(@"an error occurred while generating network request to obtain categories; %@", error);
        if ( failure ) {
            failure(error);
        }
    }
}

- (void)obtainMembersOfCategoryId:(NSNumber *)categoryId success:(void (^)(NSArray *members))success failure:(void (^)(NSError * __autoreleasing))failure {
    NSParameterAssert(categoryId);
    
    NSError *error = nil;
    NSMutableURLRequest *request = [self.sessionManager.requestSerializer requestWithMethod:@"GET" URLString:kACHostAddress parameters:@{ @"acaccomcet": categoryId } error:&error];
    __block NSError *newError = nil;
    if ( !error ) {
        NSURLSessionDataTask *task = [self.sessionManager dataTaskWithRequest:request completionHandler:^(NSURLResponse * __unused response, id responseObject, NSError *error) {
            NSAssert1(error == nil, @"an error occurred while obtaining members of category (%@)", categoryId);
            if (error) {
                NSLog(@"an error occurred while obtaining members of category (%@); %@", categoryId, error);
                if (failure) {
                    failure(error);
                }
                
            } else {
                // Let's make sure we received an array.
                if ( [responseObject isKindOfClass:[NSArray class]] ) {
                    if ( success )
                    {
                        success(responseObject);
                    }
                    
                } else {
                    // Server responded with an expected type.
                    NSLog(@"server returned an unexpected type of data in request to obtain category members; must receive an array");
                    if ( failure ) {
                        newError = [[NSError alloc] initWithDomain:ACErrorDomain code:ACBadServerResponseError userInfo:nil];
                        failure(newError);
                    }
                }
            }
        }];
        [task resume];
        
    } else {
        NSLog(@"an error occurred while generating network request to obtain category members; %@", error);
        if ( failure ) {
            failure(error);
        }
    }
}

@end
