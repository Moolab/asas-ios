//
//  ACCategory.m
//  ASAclub
//
//  Created by Rodrigo Mazzilli on 7/29/14.
//  Copyright (c) 2014 Moolab. All rights reserved.
//

#import "ACCategory.h"

@interface ACCategory ()

@property (nonatomic, assign, readwrite) int categoryId;
@property (nonatomic, copy, readwrite) NSString *name;
@property (nonatomic, copy, readwrite) NSURL *iconURL;

@end

@implementation ACCategory

- (instancetype)initWithCategoryId:(int)categoryId name:(NSString * const)name iconURL:(NSURL * const)iconURL {
    if ( ( self = [super init]) ) {
        _categoryId = categoryId;
        _name = [name copy];
        _iconURL = [iconURL copy];
    }
    return self;
}

@end
