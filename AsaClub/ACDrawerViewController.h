//
//  ACDrawerViewController.h
//  AsaClub
//
//  Created by Rodrigo Mazzilli on 7/26/14.
//  Copyright (c) 2014 Moolab. All rights reserved.
//

#import "MSDynamicsDrawerViewController.h"

@class ACMenuTableViewController;
@class ACNavigationController;

@protocol ACDrawerControllerDelegate <NSObject>

- (void)displaySetupViewController:(BOOL)animated;
- (void)displayMenuViewController;
- (void)displayHomeViewController:(BOOL)animated;
- (void)displayInboxController;

@end

@interface ACDrawerViewController : MSDynamicsDrawerViewController <ACDrawerControllerDelegate>

@property (nonatomic, strong) ACMenuTableViewController *menuViewController;
@property (nonatomic, strong) ACNavigationController *mainNavigationController;

@end
