//
//  ACCategoryCollectionViewCell.h
//  ASAclub
//
//  Created by Rodrigo Mazzilli on 7/26/14.
//  Copyright (c) 2014 Moolab. All rights reserved.
//

#import <UIKit/UIKit.h>

FOUNDATION_EXTERN NSString * const ACCategoryCollectionViewCellIdentifier;

@interface ACCategoryCollectionViewCell : UICollectionViewCell

@property (nonatomic, weak, readwrite) IBOutlet UIImageView *imageView;
@property (nonatomic, weak, readwrite) IBOutlet UILabel *categoryLabel;

@end
