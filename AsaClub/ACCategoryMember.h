//
//  ACCategoryMember.h
//  ASAclub
//
//  Created by Rodrigo Mazzilli on 8/3/14.
//  Copyright (c) 2014 Moolab. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ACCategoryMember : NSObject

@property (nonatomic, assign, readonly) int memberId;
@property (nonatomic, copy, readonly) NSString *title;
@property (nonatomic, copy, readonly) NSURL *smallImageURL;
@property (nonatomic, copy, readonly) NSURL *middleImageURL;
@property (nonatomic, copy, readonly) NSURL *largeImageURL;

- (instancetype)initWithMemberId:(int)memberId title:(NSString * const)title smallImageURL:(NSURL * const)smallImageURL middleImageURL:(NSURL * const)middleImageURL largeImageURL:(NSURL * const)largeImageURL;

@end
