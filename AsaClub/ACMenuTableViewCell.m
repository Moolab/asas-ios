//
//  ACMenuTableViewCell.m
//  AsaClub
//
//  Created by Rodrigo Mazzilli on 7/26/14.
//  Copyright (c) 2014 Moolab. All rights reserved.
//

#import "ACMenuTableViewCell.h"

@implementation ACMenuTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if ( self )
    {
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    CGRect contentViewFrame = self.contentView.frame;
    self.contentView.frame = CGRectMake(contentViewFrame.origin.x, contentViewFrame.origin.y, contentViewFrame.size.width - 44.0f, contentViewFrame.size.height);
    CGRect accessoryViewFrame = self.accessoryView.frame;
    self.accessoryView.frame = CGRectMake(accessoryViewFrame.origin.x - 44.0, accessoryViewFrame.origin.y, accessoryViewFrame.size.width, accessoryViewFrame.size.height);
}

@end
