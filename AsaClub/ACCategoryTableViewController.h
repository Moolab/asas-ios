//
//  ACCategoryTableViewController.h
//  ASAclub
//
//  Created by Rodrigo Mazzilli on 7/26/14.
//  Copyright (c) 2014 Moolab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ACCategoryTableViewController : UITableViewController

@property (nonatomic, copy, readwrite) NSNumber *categoryId;
@property (nonatomic, copy, readwrite) NSString *categoryName;

@end
