//
//  ACCategoryCollectionViewCell.m
//  ASAclub
//
//  Created by Rodrigo Mazzilli on 7/26/14.
//  Copyright (c) 2014 Moolab. All rights reserved.
//

#import "ACCategoryCollectionViewCell.h"

NSString * const ACCategoryCollectionViewCellIdentifier     =   @"ACCategoryCollectionViewCellIdentifier";

@implementation ACCategoryCollectionViewCell

@end
