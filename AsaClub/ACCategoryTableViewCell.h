//
//  ACCategoryTableViewCell.h
//  ASAclub
//
//  Created by Rodrigo Mazzilli on 7/26/14.
//  Copyright (c) 2014 Moolab. All rights reserved.
//

#import <UIKit/UIKit.h>

FOUNDATION_EXTERN NSString * const ACCategoryTableViewCellIdentifier;

@interface ACCategoryTableViewCell : UITableViewCell

@property (nonatomic, weak, readwrite) IBOutlet UIImageView *memberLogoImageView;
@property (nonatomic, weak, readwrite) IBOutlet UIButton *infoButton;

@end
