//
//  AppDelegate.h
//  AsaClub
//
//  Created by Rodrigo Mazzilli on 7/26/14.
//  Copyright (c) 2014 Moolab. All rights reserved.
//

@import UIKit;
@class ACDrawerViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) ACDrawerViewController *drawerController;

@end

