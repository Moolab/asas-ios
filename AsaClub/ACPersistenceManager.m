//
//  ACPersistenceManager.m
//  ASAclub
//
//  Created by Rodrigo Mazzilli on 7/29/14.
//  Copyright (c) 2014 Moolab. All rights reserved.
//

#import "ACPersistenceManager.h"
#import "ACHTTPManager.h"
#import "ACCategory.h"
#import "ACCategoryMember.h"
#import "FMDB.h"

static NSString * const kACDBPath                                           =   @"br.org.asaclub";
static NSString * const kACDBName                                           =   @"asaclub.sqlite3";

NSString * const ACPersistenceManagerDidSynchronizeDataNotification         =   @"ACPersistenceManagerDidSynchronizeDataNotification";
NSString * const ACPersistenceManagerSynchronizationDidFailNotification     =   @"ACPersistenceManagerSynchronizationDidFailNotification";
NSString * const ACPersistenceManagerDidSynchronizeTableKey                 =   @"ACPersistenceManagerDidSynchronizeTableKey";

typedef NS_OPTIONS(NSUInteger, ACPersistenceSyncronization) {
    ACPersistenceSyncronizationNone             =   0,      // Nothing sync'd
    ACPersistenceSyncronizationCategories       =   1 << 0, // Categorias
    ACPersistenceSyncronizationCategoryMembers  =   1 << 1  // Convênios
};

@interface ACPersistenceManager ()

// Synchronization flags
@property (nonatomic, assign, readwrite) ACPersistenceSyncronization syncronizationFlags;
// Database cache
@property (nonatomic, strong, readwrite) FMDatabaseQueue *dbQueue;

@end

@implementation ACPersistenceManager

- (void)dealloc {
    [_dbQueue close];
}

+ (instancetype)sharedInstance {
    static ACPersistenceManager *sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[ACPersistenceManager alloc] init];
    });
    return sharedInstance;
}

- (instancetype)init {
    // Reset sync flags
    _syncronizationFlags = ACPersistenceSyncronizationNone;
    // Create cache database
    NSArray *dbDirectoriesURLs = [[NSFileManager defaultManager] URLsForDirectory:NSCachesDirectory inDomains:NSUserDomainMask];
    NSAssert([dbDirectoriesURLs count] > 0, @"Unable to find location in system to store local data.");
    NSURL *cacheDirectoryURL = [dbDirectoriesURLs firstObject];
    NSURL *dbPathURL = [cacheDirectoryURL URLByAppendingPathComponent:kACDBPath];
    // Create directories, if necessary.
    NSError * __autoreleasing dbDirectoryError = nil;
    if ( [[NSFileManager defaultManager] createDirectoryAtPath:[dbPathURL path] withIntermediateDirectories:YES attributes:nil error:&dbDirectoryError] )
    {
        NSURL *dbFilePathURL = [dbPathURL URLByAppendingPathComponent:kACDBName isDirectory:NO];
        self.dbQueue = [FMDatabaseQueue databaseQueueWithPath:[dbFilePathURL path]];
        [self.dbQueue inDatabase:^(FMDatabase *db) {
            // Enable extended result codes.
            sqlite3_extended_result_codes([db sqliteHandle], 1);
        }];
        
    } else {
        NSLog(@"ASAclub could not open local data storage database.");
        // TODO: Generate exception.
    }
    
    // Create necessary tables.
    __block BOOL tablesCreated = NO;
    [self.dbQueue inDatabase:^(FMDatabase *db) {
        if ( ![db open] )
        {
            NSLog(@"ASAclub could not open local data storage database.");
            // TODO: Generate exception.
        }
        else
        {
            NSLog(@"created and opened local database at %@", dbPathURL);
        }

        // Create tables, if necessary.
        if ( ![db tableExists:@"category"] )
        {
            NSString *sql = @"CREATE TABLE category (categoryId INTEGER PRIMARY KEY, name TEXT, iconURL TEXT);";
            if ( (tablesCreated = [db executeUpdate:sql]) )
            {
                NSLog(@"table 'category' created");
            }
        }
        
        if ( ![db tableExists:@"category_member"] )
        {
            NSString *sql = @"CREATE TABLE category_member (categoryId INTEGER, idConvenio INTEGER PRIMARY KEY, titulo TEXT, imagemMenorURL TEXT, imagemMediaURL TEXT, imagemMaiorURL TEXT);";
            if ( (tablesCreated = [db executeUpdate:sql]) )
            {
                NSLog(@"table 'category_member' created");
            }
        }
    }];
    if ( !tablesCreated )
    {
        NSLog(@"ASAclub could not create local data storage database tables.");
        // TODO: Generate exception.
    }
    return self;
}

- (void)queryCategoriesWithSuccess:(void (^)(NSArray *))success failure:(void (^)(NSError *__autoreleasing))failure {
    __block NSMutableArray *categories = [NSMutableArray array];
    [self.dbQueue inDatabase:^(FMDatabase *db) {
        FMResultSet *s = [db executeQuery:@"SELECT * FROM category"];
        while ( [s next] ) {
            // Retrieve values for each record
            int categoryID = [s intForColumn:@"categoryId"];
            NSString *categoryName = [NSString stringWithUTF8String:(char *)[s UTF8StringForColumnName:@"name"]];
            NSString *iconURLString = [NSString stringWithUTF8String:(char *)[s UTF8StringForColumnName:@"iconURL"]];
            NSURL *iconURL = [NSURL URLWithString:iconURLString];
            ACCategory *category = [[ACCategory alloc] initWithCategoryId:categoryID name:categoryName iconURL:iconURL];
            [categories addObject:category];
        }
    }];
    [self.dbQueue inDatabase:^(FMDatabase *db) {
        if ( [db hadError] ) {
            dispatch_sync(dispatch_get_main_queue(), ^{
                failure([db lastError]);
            });
            
        } else {
            // Success
            dispatch_async(dispatch_get_main_queue(), ^{
                success(categories);
                // Dispatch synchronization of categories.
                if ( !(self.syncronizationFlags & ACPersistenceSyncronizationCategories) ) {
                    [self syncCategories];
                }
            });
        }
    }];
}

- (void)queryMembersOfCategoryId:(NSNumber *)categoryId success:(void (^)(NSArray *))success failure:(void (^)(NSError *__autoreleasing))failure {
    __block NSMutableArray *members = [NSMutableArray array];
    // Instantiate image URL
    NSURL *memberImageURL = [NSURL URLWithString:ACMembershipImageAddress];
    [self.dbQueue inDatabase:^(FMDatabase *db) {
        FMResultSet *s = [db executeQueryWithFormat:@"SELECT * FROM category_member WHERE idConvenio = %@", categoryId];
        while ( [s next] ) {
            // Retrieve values for each record
            int memberId = [s intForColumn:@"idConvenio"];
            NSString *title = [NSString stringWithUTF8String:(char *)[s UTF8StringForColumnName:@"titulo"]];
            // Load small image URL
            NSString *smallImageName = [NSString stringWithUTF8String:(char *)[s UTF8StringForColumnName:@"imagemMenorURL"]];
            NSURL *smallImageURL = [NSURL URLWithString:smallImageName relativeToURL:memberImageURL];
            // Load middle-sized image URL
            NSString *middleImageName = [NSString stringWithUTF8String:(char *)[s UTF8StringForColumnName:@"imagemMediaURL"]];
            NSURL *middleImageURL = [NSURL URLWithString:middleImageName relativeToURL:memberImageURL];
            // Load large image URL
            NSString *largeImageName = [NSString stringWithUTF8String:(char *)[s UTF8StringForColumnName:@"imagemMaiorURL"]];
            NSURL *largeImageURL = [NSURL URLWithString:largeImageName relativeToURL:memberImageURL];
            // Create category.
            ACCategoryMember *categoryMember = [[ACCategoryMember alloc] initWithMemberId:memberId title:title smallImageURL:smallImageURL middleImageURL:middleImageURL largeImageURL:largeImageURL];
            [members addObject:categoryMember];
        }
    }];
    [self.dbQueue inDatabase:^(FMDatabase *db) {
        if ( [db hadError] ) {
            dispatch_sync(dispatch_get_main_queue(), ^{
                failure([db lastError]);
            });
            
        } else {
            // Success
            dispatch_async(dispatch_get_main_queue(), ^{
                success(members);
                // Dispatch synchronization of categories.
                if ( !(self.syncronizationFlags & ACPersistenceSyncronizationCategoryMembers) ) {
                    [self syncMembersOfCategoryId:categoryId];
                }
            });
        }
    }];
}

- (void)syncCategories {
    [[ACHTTPManager sharedInstance] obtainCategoriesWithSuccess:^(NSArray *categories) {
        NSLog(@"server returned %d categories successfully", [categories count]);
        // Persist categories.
        __block BOOL successFlag = NO;
        __block NSError *dbError = nil;
        [self.dbQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
            // Delete all entries
            if ( [db executeUpdate:@"DELETE FROM category"] ) {
                for ( NSDictionary *category in categories ) {
                    NSNumber *categoryId = [category objectForKey:@"idCategoria"];
                    NSString *name = [category objectForKey:@"nome"];
                    NSString *iconURL = [category objectForKey:@"icon"];
                    if ( ![db executeUpdate:@"INSERT INTO category (categoryId, name, iconURL) VALUES (:categoryId, :name, :iconURL)" withParameterDictionary:@{ @"categoryId": categoryId, @"name": name, @"iconURL": iconURL }] )
                    {
                        NSLog(@"an error occurred while persisting category response; %@", [db lastErrorMessage]);
                        NSAssert([db lastErrorCode] == 0, @"an error occurred while synchronizing category response; %@", [db lastErrorMessage]);
                        dbError = [db lastError];
                        *rollback = YES;
                    }
                    else
                    {
                        successFlag = YES;
                    }
                }
                
            } else {
                // An error occurred while deleting categories entries.
                NSLog(@"an error occurred while deleting categories from database");
                *rollback = YES;
            }
        }];

        if ( successFlag ) {
            dispatch_async(dispatch_get_main_queue(), ^{
                @synchronized(self) {
                    self.syncronizationFlags |= ACPersistenceSyncronizationCategories;
                }
                // Send notification.
                [[NSNotificationCenter defaultCenter] postNotificationName:ACPersistenceManagerDidSynchronizeDataNotification object:self userInfo:@{ ACPersistenceManagerDidSynchronizeTableKey: @"category" }];
            });
            
        } else {
            NSLog(@"an error occurred while synchronizing and persisting categories; %@", dbError);
            // Send notification.
            dispatch_async(dispatch_get_main_queue(), ^{
                // Send notification.
                [[NSNotificationCenter defaultCenter] postNotificationName:ACPersistenceManagerSynchronizationDidFailNotification object:self];
            });
        }

    } failure:^(NSError *__autoreleasing error) {
        NSLog(@"a network error occurred while synchronizing categories; %@", error);
        // Send notification.
        dispatch_async(dispatch_get_main_queue(), ^{
            // Send notification.
            [[NSNotificationCenter defaultCenter] postNotificationName:ACPersistenceManagerSynchronizationDidFailNotification object:self];
        });
    }];
}

- (void)syncMembersOfCategoryId:(NSNumber *)categoryId {
    [[ACHTTPManager sharedInstance] obtainMembersOfCategoryId:categoryId success:^(NSArray *members) {
        NSLog(@"server returned %d categories members successfully", [members count]);
        // Persist members.
        __block BOOL successFlag = NO;
        __block NSError *dbError = nil;
        [self.dbQueue inTransaction:^(FMDatabase *db, BOOL *rollback) {
            // Delete all entries
            if ( [db executeUpdate:@"DELETE FROM category_member WHERE categoryId = ?", categoryId] ) {
                for ( NSDictionary *member in members ) {
                    NSNumber *memberId = [member objectForKey:@"idConvenio"];
                    NSString *titulo = [member objectForKey:@"titulo"];
                    NSString *smallImageName = [member objectForKey:@"imagemMenor"];
                    NSString *middleImageName = [member objectForKey:@"imagemMedia"];
                    NSString *largeImageName = [member objectForKey:@"imagemMaior"];
                    
                    if ( ![db executeUpdate:@"INSERT INTO category_member (categoryId, idConvenio, titulo, imagemMenorURL, imagemMediaURL, imagemMaiorURL) VALUES (:categoryId, :idConvenio, :titulo, :imagemMenorURL, :imagemMediaURL, :imagemMaiorURL)" withParameterDictionary:@{ @"categoryId": categoryId, @"idConvenio": memberId, @"titulo": titulo, @"imagemMenorURL": smallImageName, @"imagemMediaURL": middleImageName, @"imagemMaiorURL": largeImageName }] )
                    {
                        dbError = [db lastError];
                        NSAssert(dbError.code == 0, @"an error occurred while synchronizing category memmbers response; %@", dbError);
                        NSLog(@"an error occurred while persisting category members response; %@", [db lastErrorMessage]);
                        *rollback = YES;
                    }
                    else
                    {
                        successFlag = YES;
                    }
                }
                
            } else {
                // Failed to delete category members.
                NSLog(@"an error occurred while deleting category members from database");
                *rollback = YES;
            }
        }];
        
        if ( successFlag ) {
            dispatch_async(dispatch_get_main_queue(), ^{
                @synchronized(self) {
                    self.syncronizationFlags |= ACPersistenceSyncronizationCategoryMembers;
                }
                // Send notification.
                [[NSNotificationCenter defaultCenter] postNotificationName:ACPersistenceManagerDidSynchronizeDataNotification object:self userInfo:@{ ACPersistenceManagerDidSynchronizeTableKey: @"category" }];
            });
            
        } else {
            NSLog(@"an error occurred while synchronizing and persisting categories; %@", dbError);
            // Send notification.
            dispatch_async(dispatch_get_main_queue(), ^{
                // Send notification.
                [[NSNotificationCenter defaultCenter] postNotificationName:ACPersistenceManagerSynchronizationDidFailNotification object:self];
            });
        }
        
    } failure:^(NSError *__autoreleasing error) {
        NSLog(@"an error occurred while synchronizing categories; %@", error);
        // Send notification.
        dispatch_async(dispatch_get_main_queue(), ^{
            // Send notification.
            [[NSNotificationCenter defaultCenter] postNotificationName:ACPersistenceManagerSynchronizationDidFailNotification object:self];
        });
    }];
}

@end
