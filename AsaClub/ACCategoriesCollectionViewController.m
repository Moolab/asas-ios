//
//  ACCategoriesCollectionViewController.m
//  ASAclub
//
//  Created by Rodrigo Mazzilli on 7/26/14.
//  Copyright (c) 2014 Moolab. All rights reserved.
//

#import "ACCategoriesCollectionViewController.h"
#import "ACCategoryCollectionViewCell.h"
#import "ACCategoryCollectionModel.h"
#import "ACNavigationController.h"
#import "ACPersistenceManager.h"
#import "ACCategory.h"
#import "ACCategoryTableViewController.h"

#import "MRProgress.h"
#import "UIImageView+AFNetworking.h"

@interface ACCategoriesCollectionViewController ()

@property (nonatomic, strong, readwrite) ACCategoryCollectionModel *model;

@end


@implementation ACCategoriesCollectionViewController

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Start listening to synchronization notifications.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didSynchronizeModel) name:ACPersistenceManagerDidSynchronizeDataNotification object:nil];
    // Create model.
    self.model = [[ACCategoryCollectionModel alloc] init];
    [self loadModel];
}

- (void)loadModel {
    [self.model loadModelWithSuccess:^(NSArray *categories) {
        [MRProgressOverlayView dismissOverlayForView:self.view animated:YES];
        [self.collectionView reloadData];
        
    } failure:^(NSError *error) {
        [MRProgressOverlayView dismissOverlayForView:self.view animated:YES];
        
    }];
    [MRProgressOverlayView showOverlayAddedTo:self.view animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSIndexPath *indexPathForCell = [self.collectionView indexPathForCell:sender];
    ACCategory *category = [self.model.categories objectAtIndex:indexPathForCell.item];
    if ( [segue.identifier isEqualToString:@"ACCategoryToMemberListSegueIdentifier"] ) {
        ACCategoryTableViewController *viewController = [segue destinationViewController];
        [viewController setCategoryId:@(category.categoryId)];
        [viewController setCategoryName:category.name];
    }
}

#pragma mark - <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [[self.model categories] count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ACCategoryCollectionViewCell *cell = (ACCategoryCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:ACCategoryCollectionViewCellIdentifier forIndexPath:indexPath];
    // Obtain category in model.
    ACCategory *category = [self.model.categories objectAtIndex:indexPath.item];
    NSURL *imageURL = [category iconURL];
    UIImage *placeholderImage = [UIImage imageNamed:@"CategoryPlaceholder"];
    [cell.imageView setImageWithURL:imageURL placeholderImage:placeholderImage];
    cell.categoryLabel.text = [category name];
    return cell;
}

#pragma mark - Private

- (void)didSynchronizeModel {
    [self loadModel];
}

#pragma mark  - Target-Action

- (IBAction)didTouchMenuButton:(id)sender {
    // Open drawer.
    ACNavigationController *acNavigationController = (ACNavigationController *)[self navigationController];
    if ( [acNavigationController.drawerControllerDelegate respondsToSelector:@selector(displayMenuViewController)] )
    {
        [acNavigationController.drawerControllerDelegate displayMenuViewController];
    }
}

@end
