//
//  ACMenuTableViewController.h
//  AsaClub
//
//  Created by Rodrigo Mazzilli on 7/26/14.
//  Copyright (c) 2014 Moolab. All rights reserved.
//

@import UIKit;
#import "ACDrawerViewController.h"

@class ACMenuTableViewCell;

@interface ACMenuTableViewController : UITableViewController

@property (nonatomic, weak, readwrite) id <ACDrawerControllerDelegate> drawerControllerDelegate;

@property (nonatomic, weak, readwrite) IBOutlet UIView *tableHeaderView;
@property (nonatomic, weak, readwrite) IBOutlet UIImageView *userImageView;
@property (nonatomic, weak, readwrite) IBOutlet UILabel *userNameLabel;

@property (nonatomic, strong, readwrite) IBOutlet ACMenuTableViewCell *homeTableViewCell;
@property (nonatomic, strong, readwrite) IBOutlet ACMenuTableViewCell *favoritesTableViewCell;
@property (nonatomic, strong, readwrite) IBOutlet ACMenuTableViewCell *partnershipsTableViewCell;
@property (nonatomic, strong, readwrite) IBOutlet ACMenuTableViewCell *wishlistTableViewCell;
@property (nonatomic, strong, readwrite) IBOutlet ACMenuTableViewCell *foursquareTableViewCell;

@property (nonatomic, strong, readwrite) IBOutlet ACMenuTableViewCell *institutionalTableViewCell;
@property (nonatomic, strong, readwrite) IBOutlet ACMenuTableViewCell *configurationsTableViewCell;
@property (nonatomic, strong, readwrite) IBOutlet ACMenuTableViewCell *signOutTableViewCell;

@end
