//
//  ACCategoryCollectionModel.h
//  ASAclub
//
//  Created by Rodrigo Mazzilli on 7/27/14.
//  Copyright (c) 2014 Moolab. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ACCategoryCollectionModel : NSObject

@property (nonatomic, strong, readonly) NSArray *categories;

- (void)loadModelWithSuccess:(void (^)(NSArray *categories))success failure:(void (^)(NSError * __autoreleasing))failure;

@end
