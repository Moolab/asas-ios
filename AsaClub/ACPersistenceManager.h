//
//  ACPersistenceManager.h
//  ASAclub
//
//  Created by Rodrigo Mazzilli on 7/29/14.
//  Copyright (c) 2014 Moolab. All rights reserved.
//

#import <Foundation/Foundation.h>

FOUNDATION_EXTERN NSString * const ACPersistenceManagerDidSynchronizeDataNotification;
FOUNDATION_EXTERN NSString * const ACPersistenceManagerSynchronizationDidFailNotification;
FOUNDATION_EXTERN NSString * const ACPersistenceManagerDidSynchronizeTableKey;

@interface ACPersistenceManager : NSObject

+ (instancetype)sharedInstance;
- (void)queryCategoriesWithSuccess:(void (^)(NSArray *))success failure:(void (^)(NSError *__autoreleasing))failure;
- (void)queryMembersOfCategoryId:(NSNumber *)categoryId success:(void (^)(NSArray *))success failure:(void (^)(NSError *__autoreleasing))failure;

@end
