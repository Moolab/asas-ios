//
//  ACCategoryModel.m
//  ASAclub
//
//  Created by Rodrigo Mazzilli on 8/3/14.
//  Copyright (c) 2014 Moolab. All rights reserved.
//

#import "ACCategoryModel.h"
#import "ACPersistenceManager.h"

@interface ACCategoryModel ()

@property (nonatomic, copy, readwrite) NSNumber *categoryId;
@property (nonatomic, strong, readwrite) NSArray *members;

@end

@implementation ACCategoryModel

- (instancetype)initWithCategoryId:(NSNumber *)categoryId {
    NSParameterAssert(categoryId);
    if ( (self = [super init]) ) {
        _categoryId = [categoryId copy];
    }
    return self;
}

- (void)loadModelWithSuccess:(void (^)(NSArray *members))success failure:(void (^)(NSError * __autoreleasing))failure {
    if ( self.categoryId ) {
        __weak typeof(self) weakSelf = self;
        [[ACPersistenceManager sharedInstance] queryMembersOfCategoryId:self.categoryId success:^(NSArray *members) {
            weakSelf.members = members;
            if ( success ) {
                success(weakSelf.members);
            }
            
        } failure:^(NSError *error) {
            if ( failure ) {
                failure(error);
            }
        }];
        
    } else {
        @try {
            NSException *e = [NSException exceptionWithName:@"InvalidCategoryIdException" reason:@"Category model requires a valid category ID set before attempting to load it." userInfo:nil];
            @throw e;
        }
        @catch(NSException *e) {
            @throw; // rethrows e implicitly
        }
    }
}

@end
