//
//  ACHTTPManager.h
//  ASAclub
//
//  Created by Rodrigo Mazzilli on 7/26/14.
//  Copyright (c) 2014 Moolab. All rights reserved.
//

@import Foundation;
#import "AFHTTPSessionManager.h"
#import "AFNetworkReachabilityManager.h"

FOUNDATION_EXPORT NSString * const ACMembershipImageAddress;


@interface ACHTTPManager : NSObject

@property (nonatomic, strong, readonly) AFHTTPSessionManager *sessionManager;
@property (nonatomic, strong, readonly) AFNetworkReachabilityManager *reachabilityManager;

+ (instancetype)sharedInstance;
// Obtain categories
- (void)obtainCategoriesWithSuccess:(void (^)(NSArray *categories))success failure:(void (^)(NSError * __autoreleasing))failure;
- (void)obtainMembersOfCategoryId:(NSNumber *)categoryId success:(void (^)(NSArray *members))success failure:(void (^)(NSError * __autoreleasing))failure;

@end
