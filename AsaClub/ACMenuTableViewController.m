//
//  ACMenuTableViewController.m
//  AsaClub
//
//  Created by Rodrigo Mazzilli on 7/26/14.
//  Copyright (c) 2014 Moolab. All rights reserved.
//

#import "ACMenuTableViewController.h"
#import "ACMenuTableViewCell.h"

@interface ACMenuTableViewController () <UIActionSheetDelegate>

@end

@implementation ACMenuTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Prepare user image
    CALayer *imageLayer = (CALayer *)self.userImageView.layer;
    imageLayer.cornerRadius = self.userImageView.bounds.size.height / 2.0f;
    imageLayer.borderWidth = 2.0f;
    imageLayer.borderColor = [UIColor whiteColor].CGColor;
    // Set table header
    self.tableView.tableHeaderView = self.tableHeaderView;
    
    self.userImageView.image = [UIImage imageNamed:@"UserPlaceholder"];
    self.userNameLabel.text = @"Barack Obama";
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch ( section )
    {
        case 0:
            return 4;
            break;
        case 1:
            return 3;
            break;
        default:
            return 0;
            break;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *menuCell = nil;
    if ( indexPath.section == 0 )
    {
        switch ( indexPath.row )
        {
            case 0:
                menuCell = self.homeTableViewCell;
                break;
            case 1:
                menuCell = self.favoritesTableViewCell;
                break;
            case 2:
                menuCell = self.wishlistTableViewCell;
                break;
            case 3:
                menuCell = self.foursquareTableViewCell;
                break;
            default:
                break;
        }
        
    } else if ( indexPath.section == 1 )
    {
        switch ( indexPath.row )
        {
            case 0:
                menuCell = self.institutionalTableViewCell;
                break;
            case 1:
                menuCell = self.configurationsTableViewCell;
                break;
            case 2:
                menuCell = self.signOutTableViewCell;
                break;
            default:
                break;
        }
    }
    return menuCell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 20.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, tableView.bounds.size.width, 50.0f)];
    [footerView setBackgroundColor:[UIColor clearColor]];
    return footerView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( indexPath.section == 0 )
    {
        switch ( indexPath.row )
        {
            case 0:
                [self.drawerControllerDelegate displayHomeViewController:YES];
                break;
            case 1:
                [self.drawerControllerDelegate displayInboxController];
                break;
            default:
                break;
        }
    }
    else if ( indexPath.section == 1 )
    {
        switch ( indexPath.row )
        {
            case 0:
                [self promptSignOut];
                break;
            default:
                break;
        }
    }
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ( buttonIndex == [actionSheet destructiveButtonIndex] ) {
        // Perform logout
        [self.drawerControllerDelegate displaySetupViewController:YES];
    }
}

#pragma mark - Private

- (void)promptSignOut
{
    UIActionSheet *signOutActionSheet = [[UIActionSheet alloc] initWithTitle:@"Sair" delegate:self cancelButtonTitle:@"Cancelar" destructiveButtonTitle:@"Sair" otherButtonTitles:nil];
    signOutActionSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
    [signOutActionSheet showInView:self.view];
}

@end
