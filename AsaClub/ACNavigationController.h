//
//  ACNavigationController.h
//  AsaClub
//
//  Created by Rodrigo Mazzilli on 7/26/14.
//  Copyright (c) 2014 Moolab. All rights reserved.
//

@import UIKit;
#import "ACDrawerViewController.h"

@interface ACNavigationController : UINavigationController

@property (nonatomic, weak, readwrite) id <ACDrawerControllerDelegate> drawerControllerDelegate;

@end
