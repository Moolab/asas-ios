//
//  ACCategoryCollectionModel.m
//  ASAclub
//
//  Created by Rodrigo Mazzilli on 7/27/14.
//  Copyright (c) 2014 Moolab. All rights reserved.
//

#import "ACCategoryCollectionModel.h"
#import "ACPersistenceManager.h"

@interface ACCategoryCollectionModel ()

@property (nonatomic, strong, readwrite) NSArray *categories;

@end

@implementation ACCategoryCollectionModel

- (void)loadModelWithSuccess:(void (^)(NSArray *categories))success failure:(void (^)(NSError * __autoreleasing))failure {
    __weak typeof(self) weakSelf = self;
    [[ACPersistenceManager sharedInstance] queryCategoriesWithSuccess:^(NSArray *categories) {
        weakSelf.categories = categories;
        if ( success ) {
            success(weakSelf.categories);
        }
        
    } failure:^(NSError *error) {
        if ( failure ) {
            failure(error);
        }
    }];
}

@end
