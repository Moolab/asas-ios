//
//  ACDrawerViewController.m
//  AsaClub
//
//  Created by Rodrigo Mazzilli on 7/26/14.
//  Copyright (c) 2014 Moolab. All rights reserved.
//

#import "ACDrawerViewController.h"
#import "ACMenuTableViewController.h"
#import "ACNavigationController.h"


@interface ACDrawerViewController ()

@end

@implementation ACDrawerViewController

- (id)init
{
    self = [super init];
    if ( self )
    {
        // Add some example stylers
        self.menuViewController = [[ACMenuTableViewController alloc] initWithNibName:nil bundle:nil];
        self.menuViewController.drawerControllerDelegate = self;
        [self setDrawerViewController:self.menuViewController forDirection:MSDynamicsDrawerDirectionLeft];
        [self addStylersFromArray:@[[MSDynamicsDrawerScaleStyler styler], [MSDynamicsDrawerFadeStyler styler]] forDirection:MSDynamicsDrawerDirectionLeft];
        
        // Add default central panel
        UIStoryboard *centerStoryboard = [UIStoryboard storyboardWithName:@"ACDefaultStoryboard~iphone" bundle:nil];
        self.mainNavigationController = [centerStoryboard instantiateInitialViewController];
        self.mainNavigationController.drawerControllerDelegate = self;
        [self displayHomeViewController:NO];
//        
//        if ( ![[ACAuthenticationManager sharedInstance] isUserAuthenticated] || ![ADUserMO user] ) {
//            [self displaySetupViewController:NO];
//            
//        } else {
//            [self displayHomeViewController:NO];
//            ADUserMO *userMO = [ADUserMO user];
//            NSString *tokenData = [[FBSession activeSession].accessTokenData accessToken];
//        }
    }
    return self;
}

- (void)initializeWithFBUserId:(NSString *)fbUserId token:(NSString *)token
{
 #ifdef __IPHONE_8_0
    // Register for remote notification.
    if ( [[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)] )
    {
        // iOS >= 8.0
        // Ask user for permission to display user notifications.
        UIUserNotificationType types = UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound;
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        // Register for remote notification.
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        // iOS < 8.0
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound)];
    }
#else
    // iOS < 8.0
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound)];
#endif
}

#pragma mark - ADDrawerControllerDelegate

- (void)displaySetupViewController:(BOOL)animated
{
    // Disable drag gesture until user has signed in.
    [self setPaneDragRevealEnabled:NO forDirection:MSDynamicsDrawerDirectionLeft];
    // Set setup controller in pane.
//    ACSetupViewController *setupViewController = [[ADSetupViewController alloc] init];
//    setupViewController.setupDelegate = self;
//    [self setPaneViewController:setupViewController animated:animated completion:nil];
}

- (void)displayMenuViewController
{
    [self setPaneState:MSDynamicsDrawerPaneStateOpen inDirection:MSDynamicsDrawerDirectionLeft animated:YES allowUserInterruption:NO completion:nil];
}

- (void)displayHomeViewController:(BOOL)animated
{
    [self setPaneViewController:self.mainNavigationController animated:animated completion:nil];
}

- (void)displayInboxController
{
}

#pragma mark - ACSetupViewControllerDelegate

//- (void)didCompleteSetup:(ADSetupViewController *)controller user:(id<FBGraphUser>)user accessToken:(NSString *)accessToken
//{
//    ADUserMO *userMO = [ADUserMO user];
//    // Initialize
//    [self initializeWithFBUserId:userMO.facebookId token:accessToken];
//    // Enable drag gesture for menu drawer.
//    [self setPaneDragRevealEnabled:YES forDirection:MSDynamicsDrawerDirectionLeft];
//    [self displayHomeViewController:YES];
//}
//
//- (void)controller:(ADSetupViewController *)controller didFail:(NSError *)error
//{
//    UIAlertView *aboutFacebookRequirement = [[UIAlertView alloc] initWithTitle:@"Acesso ao Facebook" message:@"A conexão com o seu Facebook é necessária.\nVerifique a sua configuração no aplicativo e tente novamente.\nAguardamos você!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//    [aboutFacebookRequirement show];
//}

@end
