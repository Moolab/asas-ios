//
//  ACCategoryMember.m
//  ASAclub
//
//  Created by Rodrigo Mazzilli on 8/3/14.
//  Copyright (c) 2014 Moolab. All rights reserved.
//

#import "ACCategoryMember.h"

@implementation ACCategoryMember

- (instancetype)initWithMemberId:(int)memberId title:(NSString * const)title smallImageURL:(NSURL * const)smallImageURL middleImageURL:(NSURL * const)middleImageURL largeImageURL:(NSURL * const)largeImageURL {
    if ( ( self = [super init]) ) {
        _memberId = memberId;
        _title = [title copy];
        _smallImageURL = [smallImageURL copy];
        _middleImageURL = [middleImageURL copy];
        _largeImageURL = [largeImageURL copy];
    }
    return self;
}

@end
