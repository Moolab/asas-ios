//
//  ACCategoriesCollectionViewController.h
//  ASAclub
//
//  Created by Rodrigo Mazzilli on 7/26/14.
//  Copyright (c) 2014 Moolab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ACCategoriesCollectionViewController : UICollectionViewController

@end
